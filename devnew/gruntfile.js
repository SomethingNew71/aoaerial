/* ===================================================== */
/* Component:                   Grunt configuration file */
/* ===================================================== */

module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    //JS concat
    concat: {
      lib: {
        src: ['src/lib/js/*.js'],
        dest: 'dist/js/lib.min.js'
      },
      app: {
        src: ['src/app/js/app.js'],
        dest: 'dist/js/app.min.js'
      }
    },
    //JS Uglificaiton
    uglify: {
      lib: {
        src: 'dist/js/lib.min.js',
        dest: 'dist/js/lib.min.js'
      },
      app: {
        src: 'dist/js/app.min.js',
        dest: 'dist/js/app.min.js'
      }
    },
    //Image Minificaiton
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/images/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'dist/images/'
        }]
      }
    },
    //SCSS/SASS Compression
    sass: {
      lib: {
        options: {
          style: 'compressed'
        },
        files: {
          'dist/css/lib.min.css': 'src/lib/scss/lib.scss',
        }
      },
      app: {
        options: {
          style: 'compressed'
        },
        files: {
          'dist/css/app.min.css': 'src/app/scss/app.scss',
        }
      } 
    },
    //Post CSS processes helping compatibility
    postcss: {
      options: {
        map: true,
        processors: [
          require('pixrem')(),
          require('autoprefixer')({browsers: 'last 2 versions'}),
          require('cssnano')()
        ]
      },
      app: {
        src: 'dist/css/app.min.css'
      },
      lib: {
        src: 'dist/css/lib.min.css'
      }
    },
    //HTML Minification
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'dist/index.html': 'src/index.html'
        }
      }
    },
    //File Copy Task
    copy: {
      main: {
        src: 'src/index.html',
        dest: 'dist/index.html',
      }
    },
    //Watch Task for development
    watch: {
      scripts: {
        files: ['src/app/js/*.js'],
        tasks: ['concat', 'uglify'],
        options: {
          spawn: false,
          livereload: true,
        },
      },
      css: {
        files: ['src/app/scss/*.scss'],
        tasks: ['sass', 'postcss'],
        options: {
          spawn: false,
          livereload: true,
        }
      },
      html: {
        files: ['src/index.html'],
        tasks: ['copy']
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-serve');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-devtools');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.registerTask('default', ['concat', 'uglify', 'sass', 'imagemin', 'htmlmin']);
  grunt.registerTask('dist', ['concat', 'uglify', 'sass', 'postcss', 'imagemin', 'htmlmin']);
  grunt.registerTask('dev', ['watch']);
};
