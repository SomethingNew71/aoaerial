/* ==============================================
 main flex slider
 =============================================== */
$(window).load(function () {
  $('.main-flex-slider').flexslider({
    slideshowSpeed: 5000,
    directionNav: false,
    animation: "fade",
    controlNav: false
  });
});

/* ==============================================
 Sticky Navbar
 =============================================== */

$(document).ready(function () {
  $(".navbar").sticky({topSpacing: 0});
});


/* ==============================================
 Auto Close Responsive Navbar on Click
 =============================================== */

// function close_toggle() {
//     if ($(window).width() <= 768) {
//         $('.navbar-collapse a').on('click', function () {
//             $('.navbar-collapse').collapse('hide');
//         });
//     }
//     else {
//         $('.navbar .navbar-default a').off('click');
//     }
// }
// close_toggle();

// $(window).resize(close_toggle);


//owl carousel for testimonials
$(document).ready(function () {

  $("#testi-carousel").owlCarousel({
    // Most important owl features
    items: 1,
    itemsCustom: false,
    itemsDesktop: [1199, 1],
    itemsDesktopSmall: [980, 1],
    itemsTablet: [768, 1],
    itemsTabletSmall: false,
    itemsMobile: [479, 1],
    singleItem: false,
    startDragging: true,
    autoPlay: 13000
  });
  $("#work-slide").owlCarousel({
    // Most important owl features
    items: 1,
    navigation: true,
    itemsCustom: false,
    itemsDesktop: [1199, 1],
    itemsDesktopSmall: [980, 1],
    itemsTablet: [768, 1],
    itemsTabletSmall: false,
    itemsMobile: [479, 1],
    singleItem: false,
    startDragging: true,
    autoPlay: false
  });
});

/*=========================*/
/*========portfolio mix====*/
/*==========================*/
// $('#grid').mixitup();

/* ==============================================
 WOW plugin triggers animate.css on scroll
 =============================================== */

var wow = new WOW(
    {
      boxClass: 'wow', // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset: 100, // distance to the element when triggering the animation (default is 0)
      mobile: false        // trigger animations on mobile devices (true is default)
    }
);
wow.init();


//MAGNIFIC POPUP
// $('.show-image').magnificPopup({type: 'image'});



//smooth scroll
$(function () {
  $('.scrollto a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 50
        }, 1000);
        return false;
      }
    }
  });
});

/* ==============================================
 Vide.js for easy HTML5 video backgrounds
 =============================================== */

$('#home').vide({
  mp4: 'img/promovideo.mp4',
  webm: 'img/promovideo.webm',
  ogv: 'img/promovideo.ogv',
  poster: 'img/header.jpg'
});


  